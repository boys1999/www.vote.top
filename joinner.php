<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>投票</title>
    <meta name="format-detection" content="telephone=no">
    <meta content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" name="viewport">
    <meta name="apple-mobile-web-app-status-bar-style" content="black"/>
    <meta name="apple-mobile-web-app-title" content="">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link rel="stylesheet" href="public/qdd/mb/qdd.css">
    <link rel="stylesheet" href="vote/css/common.css">
    <link rel="stylesheet" href="vote/css/joinner.css?52">
    <link rel="stylesheet" href="vote/iconfont/iconfont.css" type="text/css" />
</head>

<body>
    <div id="vote" >
        <!-- 公告 -->
        <div class="notice">
            <div class="row">
                <div class="left">公告：</div>
                <div class="right">
                    <i class="icon iconfont icon-shijian"></i>
                </div>
                <div class="center">
                    <marquee scrollAmount=5 width='100%'>酒吧横向公告酒吧横向吧横向公告1</marquee>
                </div>
            </div>
        </div>
        <!-- /公告 -->
        <!-- 图片轮播 -->
        <div class="y-addWrap">
            <div class="swipe" id="mySwipe">
                <div class="swipe-wrap">
                    <div v-for="(item,index) in swipeItem">
                        <a v-bind:href="item.href">
                            <img class="img-responsive" v-bind:src="item.img"/>
                        </a>
                    </div>
                </div>
            </div>
            <ul id="y-position" style="display: none;">
                <li v-bind:class="[index == 0 ? 'cur' : '']" v-for="(item,index) in swipeItem"></li>
            </ul>
        </div>
        <!-- /图片轮播 -->

        <!-- 选手信息 -->
        <div class="joinner shadow">
            <div class="intop">
                <div class="left">
                    <div class="index">001</div>
                    <div class="top">
                        <div class="label">排名</div>
                        <div class="value">12456</div>
                    </div>
                </div>
                <div class="right">
                    <div class="g1">
                        <div class="name">Carmen</div>
                        <div class="region">茂南赛区</div>
                    </div>
                    <div class="g2 gradientbg">
                        <a @click="direct" href="javascript:void(0)">·直播间</a><a @click="mv" href="javascript:void(0)">·MV</a><a @click="audition" href="javascript:void(0)">·歌曲试听</a>
                    </div>
                    <div class="g3" @click="votefor">
                        <div class="votefor">
                            <i class="icon iconfont icon-shijian"></i>
                            <div class="label">投TA</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="line gradientbg"></div>
            <div class="diff">
                <div class="prev">与前一名相差000票</div>
                <div class="dot">·</div>
                <div class="next">与后一名相差000票</div>
            </div>
        </div>
        <!-- /选手信息 -->

        <!-- 投票信息 -->
        <div class="votedata">
            <ul>
                <li>
                    <i class="icon iconfont icon-shijian"></i>
                    <span class="label">123</span>
                </li>
                <li>
                    <i class="icon iconfont icon-shijian"></i>
                    <span class="label">123</span>
                </li>
                <li>
                    <i class="icon iconfont icon-shijian"></i>
                    <span class="label">123</span>
                </li>
            </ul>
        </div>
        <!-- /投票信息 -->

        <!-- 投我的人 -->
        <div class="voteme">
            <div class="head">
                <div class="line"></div>
                <div class="label">
                    <i class="icon iconfont icon-shijian"></i>
                    <span>投TA的人</span>
                </div>
            </div>
            <ul class="list">
              <li>
                  <img src="https://g-search2.alicdn.com/img/bao/uploaded/i4/i4/2762292769/TB2LqZNqFXXXXXQXXXXXXXXXXXX_!!2762292769.jpg_640x640.jpg_.webp">
                  <div class="label">马云</div>
              </li>
              <li>
                  <img src="https://g-search2.alicdn.com/img/bao/uploaded/i4/i4/2762292769/TB2LqZNqFXXXXXQXXXXXXXXXXXX_!!2762292769.jpg_640x640.jpg_.webp">
                  <div class="label">马云</div>
              </li>
              <li>
                  <img src="https://g-search2.alicdn.com/img/bao/uploaded/i4/i4/2762292769/TB2LqZNqFXXXXXQXXXXXXXXXXXX_!!2762292769.jpg_640x640.jpg_.webp">
                  <div class="label">马云马云马云马云马云马云马云马云马云</div>
              </li>
              <li>
                  <img src="https://g-search2.alicdn.com/img/bao/uploaded/i4/i4/2762292769/TB2LqZNqFXXXXXQXXXXXXXXXXXX_!!2762292769.jpg_640x640.jpg_.webp">
                  <div class="label">马云</div>
              </li>
              <li>
                  <img src="https://g-search2.alicdn.com/img/bao/uploaded/i4/i4/2762292769/TB2LqZNqFXXXXXQXXXXXXXXXXXX_!!2762292769.jpg_640x640.jpg_.webp">
                  <div class="label">马云</div>
              </li>
            </ul>
            <div class="more">
                <a class="qdd-btn" href="javascript:void(0)">
                    <i class="icon iconfont icon-shijian"></i>
                    <span>点击查看更多</span>
                </a>
            </div>
        </div>
        <div class="linemod"></div>
        <!-- /投我的人 -->

        <!-- 送礼的人 -->
        <div class="giftsme">
            <div class="head">
                <div class="line gradientbg"></div>
                <div class="label">
                    <i class="icon iconfont icon-shijian"></i>
                    <span>送TA礼物的人</span>
                </div>
            </div>
            <ul class="list">
              <li>
                  <img src="https://g-search2.alicdn.com/img/bao/uploaded/i4/i4/2762292769/TB2LqZNqFXXXXXQXXXXXXXXXXXX_!!2762292769.jpg_640x640.jpg_.webp">
                  <span>马云送化腾10个话筒</span>
              </li>
              <li>
                  <img src="https://g-search2.alicdn.com/img/bao/uploaded/i4/i4/2762292769/TB2LqZNqFXXXXXQXXXXXXXXXXXX_!!2762292769.jpg_640x640.jpg_.webp">
                  <span>马云送化腾10个话筒</span>
              </li>
              <li>
                  <img src="https://g-search2.alicdn.com/img/bao/uploaded/i4/i4/2762292769/TB2LqZNqFXXXXXQXXXXXXXXXXXX_!!2762292769.jpg_640x640.jpg_.webp">
                  <span>马云送化腾10个话筒</span>
              </li>
            </ul>
            <div class="more">
                <a class="qdd-btn" href="javascript:void(0)">
                    <i class="icon iconfont icon-shijian"></i>
                    <span>点击查看更多</span>
                </a>
            </div>
        </div>
        <div class="linemod"></div>
        <!-- /送礼的人 -->


        <!-- 广告区1 -->
        <div class="adv1"></div>
        <!-- /广告区1 -->

        <br/>
        <!-- 活动名称 -->
        <div class="activity">
            <div class="line gradientbg1"></div>
            <div class="label">活动名称</div>
        </div>
        <!-- /活动名称 -->

        <!-- 返回顶部 -->
        <div class="backtop" v-on:click="backtop()">
            <div class="wrap">
                <div class="label">返回顶部</div>
            </div>
        </div>
        <!-- /返回顶部 -->

        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>

        <!-- 底部导航 -->
        <div class="menu">
            <ul>
                <li v-for="(item,index) in menu">
                    <a v-bind:href="item.href">
                        <i class="icon iconfont" v-bind:class="item.icon"></i><br/>
                        <label>{{item.label}}</label>
                    </a>
                </li>
            </ul>
        </div>
        <!-- /底部导航 -->
    <script src="public/vue/vue.min.js" type="text/javascript"></script>
    <script src="public/axios/axios.min.js"></script>
    <script src="public/swipe/swipe.min.js"></script>
    <script src="public/layer_mobile/layer.js"></script>
    <script src="public/qdd/mb/qdd.js"></script>
    <!-- temp mock data -->
    <script src="public/mockjs/dist/mock-min.js" type="text/javascript"></script>
    <script src="public/mockjs/dist/mock-extend.js" type="text/javascript"></script>
    <script src="vote/js/mock.js" type="text/javascript"></script>
    <!-- /temp mock data -->
    <script src="vote/js/common.js" type="text/javascript"></script>
    <script src="vote/js/joinner.js" type="text/javascript"></script>


</body>

</html>
