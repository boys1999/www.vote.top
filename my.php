<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>我的</title>
    <meta name="format-detection" content="telephone=no">
    <meta content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" name="viewport">
    <meta name="apple-mobile-web-app-status-bar-style" content="black"/>
    <meta name="apple-mobile-web-app-title" content="">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link rel="stylesheet" href="public/qdd/mb/qdd.css">
    <link rel="stylesheet" href="vote/css/common.css">
    <link rel="stylesheet" href="vote/css/my.css">
    <link rel="stylesheet" href="vote/iconfont/iconfont.css" type="text/css" />
</head>
<body>
    <div id="vote">
        <!-- 头部 -->
        <div class="header">
            <div class="left">
                <div class="logo">
                    <img src="https://g-search1.alicdn.com/img/bao/uploaded/i4/i4/1074906482/TB2WNhgqFXXXXbPXXXXXXXXXXXX_!!1074906482.jpg_640x640.jpg_.webp">
                </div>
            </div>
            <div class="right">
                <i class="icon iconfont icon-shijian"></i>
            </div>
            <div class="center">
                <div class="value">马化腾</div>
            </div>
        </div>
        <!-- /头部 -->
        <!-- 三列 -->
        <div class="assets">
            <ul>
              <li>
                  <div class="value">666</div>
                  <div class="label">积分</div>
              </li>
              <li>
                  <div class="value">666</div>
                  <div class="label">消费</div>
              </li>
              <li>
                  <div class="value">666</div>
                  <div class="label">票数</div>
              </li>
            </ul>
        </div>
        <!-- /三列 -->
        <!-- 九格 -->
        <div class="itmes">
            <ul>
                <li>
                    <a href="javascript:void(0)">
                        <div class="logo"> <i class="icon iconfont icon-shijian"></i>
                        </div>
                        <div class="label">礼物</div>
                        <span class="tips">1</span>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <div class="logo"> <i class="icon iconfont icon-shijian"></i>
                        </div>
                        <div class="label">消息</div>
                        <span class="tips">001</span>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <div class="logo"> <i class="icon iconfont icon-shijian"></i>
                        </div>
                        <div class="label">评论</div>
                        <span class="tips">001</span>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <div class="logo"> <i class="icon iconfont icon-shijian"></i>
                        </div>
                        <div class="label">报名</div>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <div class="logo"> <i class="icon iconfont icon-shijian"></i>
                        </div>
                        <div class="label">客服</div>
                    </a>
                </li>
                <li>
                    <a href="index.php">
                        <div class="logo"> <i class="icon iconfont icon-shijian"></i>
                        </div>
                        <div class="label">首页</div>
                    </a>
                </li>
            </ul>
        </div>
        <!-- /九格 -->
    </div>
    <script src="public/vue/vue.min.js" type="text/javascript"></script>
    <script src="public/axios/axios.min.js"></script>
    <script src="public/swipe/swipe.min.js"></script>
    <script src="public/layer_mobile/layer.js"></script>
    <script src="public/qdd/mb/qdd.js"></script>
    <!-- temp mock data -->
    <script src="public/mockjs/dist/mock-min.js" type="text/javascript"></script>
    <script src="public/mockjs/dist/mock-extend.js" type="text/javascript"></script>
    <script src="vote/js/mock.js" type="text/javascript"></script>
    <!-- /temp mock data -->
    <script src="vote/js/common.js" type="text/javascript"></script>
    <script src="vote/js/my.js" type="text/javascript"></script>
</body>
</html>