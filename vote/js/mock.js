// 延迟1至2毫米之间返回数据
Mock.setup({
    timeout: '10-20'
});
// 模块数据
var apiMock=[
    // api: 请求图片轮播
    {
        'url':/swipe\/list(.*)/,
        'type':'get',
        'request':{
            'id':52
        },
        'response':{
            'status':200,
            'data|3':[{
                'href': 'javascript:void(0)',
                'img':'@headimg',
                'dsc':'@cparagraph(1,1)'
            }]
        }
    },
    // api: 选手列表
    {
        'url':/vote\/votelist(.*)/,
        'type':'get',
        'request':{
            'id':52
        },
        'response':{
            'status':200,
            'data|17':[{
                'href': 'javascript:void(0)',
                'view':'@integer(200,30000)',
                'vote':'@integer(200,30000)',
                'index':'@integer(1,100)',
                'img':'@headimg',
                'name':'@cname'
            }]
        }
    },
    // api: 选手列表
    {
        'url':/vote\/menulist(.*)/,
        'type':'get',
        'request':{
            'id':52
        },
        'response':{
            'status':200,
            'data':[{
                "href":"index.php",
                "icon":"icon-erweima",
                "label":"首页"
            },{
                "href":"joinner.php",
                "icon":"icon-erweima",
                "label":"选手"
            },{
                "href":"ranking.php",
                "icon":"icon-shijian",
                "label":"排行"
            },{
                "href":"signup.php",
                "icon":"icon-shijian",
                "label":"报名"
            },{
                "href":"my.php",
                "icon":"icon-shijian",
                "label":"我的"
            }]
        }
    }
]
// 拦截ajax并返回模拟数据
for (var i = 0; i < apiMock.length; i++) {
    Mock.mock(apiMock[i].url,apiMock[i].type, apiMock[i].response);
};


// ,
// 'title': '@ctitle(1,3)',                                        //标题，取1至3字内
// 'text': '@cparagraph(1,3)',                                     //文本，取1至3句话，每句话13字
// 'headimg': '@headimg',                                          //头像
// 'number':'@integer(1, 3)',                                      //随机整数
// 'email':'@email',                                               //邮箱
// 'ip':'@ip',                                                     //id
// 'region':'@region',                                             //区，华南，华东等
// 'province':'@province',                                         //省
// 'city':'@city',                                                 //城
// 'county':'@county(true)',                                       //true 则返回 城区及省，否则只返回镇
// 'float':'@float(60, 100, 3, 5)',                                //@float(min, max, dmin(小数点位数), dmax(小数点位数))
// 'datetime':'@datetime("yyyy-M-d H:m:s")',                       //日期
// 'obj|2-3':{                                                     //obj 从对象随机选出2至3个属性
// 'id1':1,
// 'id2':2,
// 'id3':3,
// 'id4':4,
// },
// 'arr|3':[{                                                      // arr 随机生成3个数组内的元素
// 'id':'@integer(1, 50)'
// }]