// http 请求回数据
var HttpData={
};
// vue渲染数据
var VueData={
};
// http集中请求数据后处理
axios.all([])
    .then(axios.spread(function() {
        var Vote = new Vue({
            el: '#vote',
            data: VueData,
            methods:{
            }
        });
        // vue 渲染后执行
        Vote.$nextTick(function() {
            layer.closeAll(); //关闭加载弹窗提示
            after_rendering();
        });
    }))
    .catch(function (error) {
        console.log(error);
    });
// vue 渲染后执行
function after_rendering(){
    // 渲染完成显示
    document.querySelector('#vote').style="block";
};