// http 请求回数据
var HttpData={
    swipeItem:{},
    menulist:{}
};
// vue渲染数据
var VueData={
    swipeItem:[],
    menu:[],
    loadTips:'下拉加载更多'
};
// http集中请求数据后处理
axios.all([http_swipe(),http_menulist()])
    .then(axios.spread(function(swipeItem,menulist) {
        HttpData.swipeItem=swipeItem;
        VueData.menu=menulist.data.data;
        VueData.swipeItem=swipeItem.data.data;
        var Vote = new Vue({
            el: '#vote',
            data: VueData,
            methods:{
                voteTips:vote_tips,
                backtop:back_top
            }
        });
        // vue 渲染后执行
        Vote.$nextTick(function() {
            layer.closeAll(); //关闭加载弹窗提示
            after_rendering();
        });
    }))
    .catch(function (error) {
        console.log(error);
    });
// vue 渲染后执行
function after_rendering(){
    // 渲染完成显示
    document.querySelector('#vote').style="block";

    //图片轮播
    var bullets = document.getElementById('y-position').getElementsByTagName('li');
    var banner = Swipe(document.getElementById('mySwipe'), {
        auto: 2000,
        continuous: true,
        disableScroll: false,
        callback: function(pos) {
            var i = bullets.length;
            while (i--) {
                bullets[i].className = '';
            }
            bullets[pos].className = 'cur';
        }
    });
};





