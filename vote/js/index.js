// http 请求回数据
var HttpData={
    votelist:{},
    swipeItem:{},
    menulist:{}
};
// vue渲染数据
var VueData={
    votelist:[],
    swipeItem:[],
    menu:[],
    loadTips:'下拉加载更多'
};
// http集中请求数据后处理
axios.all([http_swipe(),http_votelist(),http_menulist()])
    .then(axios.spread(function(swipeItem,votelist,menulist) {
        console.table(votelist.data.data);
        HttpData.votelist=votelist;
        HttpData.swipeItem=swipeItem;
        VueData.menu=menulist.data.data;
        page_vote();
        VueData.swipeItem=swipeItem.data.data;
        var Vote = new Vue({
            el: '#vote',
            data: VueData,
            methods:{
                backtop:back_top,
                votefor:function(voteid,index){
                    http_votefor(voteid,index,this.votelist);
                },
                voteTips:vote_tips,
            }
        });
        // vue 渲染后执行
        Vote.$nextTick(function() {
            layer.closeAll(); //关闭加载弹窗提示
            after_rendering();
        });
    }))
    .catch(function (error) {
        // QDD.pageFail();
        console.log(error);
    });

// 投一票
function http_votefor(voteid,index,array){
    array[index].vote+=1;
    Vue.set(array, index, array[index]);
    tips_vote(1);
};
// 投票者数据设置分页
var pageVote={};// 分页数据
function page_vote(){
    if ( typeof pageVote.page =='undefined') {
        var page=1;
    }else{
        var page=pageVote.page+1;
        if (page>pageVote.totalPage) {
            VueData.loadTips="已到底，没有更多选手了";
            return false;
        };
    };
    // 得到指定页数据
    pageVote=QDD.jsonSql({
        array:HttpData.votelist.data.data,
        page:{
            page:page,
            perPage:3,
        }
    });
    VueData.votelist =QDD.arrCrossover(VueData.votelist.concat(pageVote.data));
};

// vue 渲染后执行
function after_rendering(){
    // 渲染完成显示
    document.querySelector('#vote').style="block";
    // 选手列表：加载更多
    window.onscroll = function() {
        if (QDD.getScrollTop() + QDD.getWindowHeight() == QDD.getScrollHeight()) {
            page_vote();
        };
    };
    //图片轮播
    var bullets = document.getElementById('y-position').getElementsByTagName('li');
    var banner = Swipe(document.getElementById('mySwipe'), {
        auto: 2000,
        continuous: true,
        disableScroll: false,
        callback: function(pos) {
            var i = bullets.length;
            while (i--) {
                bullets[i].className = '';
            }
            bullets[pos].className = 'cur';
        }
    });
};





