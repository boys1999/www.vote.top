// vue渲染数据
var VueData={
    votelist:[],
    swipeItem:[],
    menu:[],
    loadTips:'下拉加载更多'
};
// http 请求回数据
var HttpData={
};
// http集中请求数据后处理
axios.all([http_swipe(),http_votelist(),http_menulist()])
    .then(axios.spread(function(swipeItem,votelist,menulist) {
        VueData.swipeItem=swipeItem.data.data;
        VueData.menu=menulist.data.data;
        var Vote = new Vue({
            el: '#vote',
            data: VueData,
            methods:{
                votefor:function(voteid,index){
                    http_votefor();
                },
                audition:function(){
                    (function(){
                        layer.open({
                            title:[
                                '试听马云的歌声',
                                'background:rgba(0,0,0,0.999);color:#fff'
                            ],
                            content:    `<div>
                                            <audio autoplay="autoplay" style="width:100%;" controls src="http://dx.sc.chinaz.com/Files/DownLoad/sound1/201705/8728.mp3">
                                        </div>`,
                            style:"background:#fff;",
                            anim:'scale',
                            shade:true,
                            shadeClose:true
                        });
                    })();
                },
                direct:function(){
                    (function(){
                        layer.open({
                            title:[
                                '马云的演讲',
                                'background:rgba(0,0,0,0.999);color:#fff'
                            ],
                            content:    `<div>
                                            <video controls="controls" style="width:100%;" autoplay="autoplay">
                                                <source src="http://www.w3school.com.cn/i/movie.ogg" type="video/ogg" />
                                                <source src="http://www.w3school.com.cn/i/movie.mp4" type="video/mp4" />
                                                Your browser does not support the video tag.
                                            </video>
                                        </div>`,
                            style:"background:#fff;",
                            anim:'scale',
                            shade:true,
                            shadeClose:true
                        });
                    })();
                },
                mv:function(){
                    (function(){
                        layer.open({
                            title:[
                                '马云的演讲',
                                'background:rgba(0,0,0,0.999);color:#fff'
                            ],
                            content:    `<div>
                                            <video controls="controls" style="width:100%;" autoplay="autoplay">
                                                <source src="http://www.w3school.com.cn/i/movie.ogg" type="video/ogg" />
                                                <source src="http://www.w3school.com.cn/i/movie.mp4" type="video/mp4" />
                                                Your browser does not support the video tag.
                                            </video>
                                        </div>`,
                            style:"background:#fff;",
                            anim:'scale',
                            shade:true,
                            shadeClose:true
                        });
                    })();
                },
                backtop:back_top
            }
        });
        // vue 渲染后执行
        Vote.$nextTick(function() {
            layer.closeAll();//关闭加载弹窗提示
            after_rendering();
        });
    }))
    .catch(function (error) {
        // QDD.pageFail();
        console.log(error);
    });

// 投一票
function http_votefor(){
    tips_vote(1);
};

// vue 渲染后执行
function after_rendering(){
    // 渲染完成显示
    document.querySelector('#vote').style="block";
    //图片轮播
    var bullets = document.getElementById('y-position').getElementsByTagName('li');
    var banner = Swipe(document.getElementById('mySwipe'), {
        auto: 2000,
        continuous: true,
        disableScroll: false,
        callback: function(pos) {
            var i = bullets.length;
            while (i--) {
                bullets[i].className = '';
            }
            bullets[pos].className = 'cur';
        }
    });
};