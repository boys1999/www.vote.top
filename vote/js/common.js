// 图片轮播列表
function http_swipe() {
    return axios.get('swipe/lists');
}
function tips_vote(index){
    if (index==1) {
        QDD.tips({
            content:'投票成功！感谢您的支持',
            shade:false,
            icon:'02_1.gif',
            anim:'up',
            style:"background:rgba(0,0,0,0.8);color:#fff;box-shadow:0 0 10px rgba(0,0,0,0.3);",
            time:3
        });
    };
};
// 参赛者列表
function http_votelist() {
    return axios.get('vote/votelist');
}
// 底部菜单栏列表
function http_menulist() {
    return axios.get('vote/menulist');
}
// 返回顶部
function back_top(){
    window.scrollTo(0,0);
};
// 活动说明
function vote_tips(){
    var htmlTips = "本次活动说明";
    layer.open({
        content: htmlTips,
        btn: '联知道了！'
    });
}