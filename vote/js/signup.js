// http 请求回数据
var HttpData={
    swipeItem:{},
    menulist:{}
};
// vue渲染数据
var VueData={
    swipeItem:[],
    menu:[],
    loadTips:'下拉加载更多'
};
// http集中请求数据后处理
axios.all([http_swipe(),http_menulist()])
    .then(axios.spread(function(swipeItem,menulist) {
        HttpData.swipeItem=swipeItem;
        VueData.menu=menulist.data.data;
        VueData.swipeItem=swipeItem.data.data;
        var Vote = new Vue({
            el: '#vote',
            data: VueData,
            methods:{
                backtop:back_top,
                voteTips:vote_tips,
                uploadImg:function(){
                    if ($('#J_imgbox li').length>=4) {
                        layer.open({
                            content: '最多只能上传4张图片',
                            skin: 'msg',
                            time:3
                        });
                        return false;
                    };
                    $("#inputfile_upload").trigger('click');
                },
                uploadDemo:function(){
                    alert(    '上传DEMO'   );
                },
                soundRecording:function(){
                    alert(    '录音'   );
                },
                uploadVideoOnline:function(){
                    alert(    '上传在线视频'   );
                },
                uploadVideoLocal:function(){
                    alert(    '上传本地视频'   );
                },
                videoTips:function(vtype){
                        if (vtype=='local') {
                            var htmlTips="在线视频上传说明";
                        }else{
                            var htmlTips="本地视频上传说明";
                        };
                        layer.open({
                            content: htmlTips,
                            btn:'联知道了！'
                        });
                }
            }
        });
        // vue 渲染后执行
        Vote.$nextTick(function() {
            layer.closeAll(); //关闭加载弹窗提示
            after_rendering();
        });
    }))
    .catch(function (error) {
        console.log(error);
    });
// vue 渲染后执行
function after_rendering(){
    // 渲染完成显示
    document.querySelector('#vote').style="block";

    //图片轮播
    var bullets = document.getElementById('y-position').getElementsByTagName('li');
    var banner = Swipe(document.getElementById('mySwipe'), {
        auto: 2000,
        continuous: true,
        disableScroll: false,
        callback: function(pos) {
            var i = bullets.length;
            while (i--) {
                bullets[i].className = '';
            }
            bullets[pos].className = 'cur';
        }
    });
    // 验证表单
    $("#J_formsignup").validate({
        debug: true, // debug，只验证不提交表单
        ignore: '.ignore', // 忽略不验证有此class的表单
        rules: {
            realname: {
                required: true,
                rangelength: [2, 8]
            },
            mobile: {
                required: true,
                isMobile: true,
                remote: {
                    url: "ajax.php",// 远程返回字符 true或false
                    type: "post"
                }
            }
        },
        messages: {
            realname: {
                required: "请输入姓名",
                rangelength: $.validator.format("姓名长度为{0}-{1}个字符")
            },
            mobile: {
                required: "请输入手机号码",
                isMobile: "请正确填写您的手机号码",
                remote: "该手机号码已经报名！"
            }
        }
    });
    // 上传图片
    $("#inputfile_upload").change(function() {
        layer.open({
            content: '图片上传中…',
            time:10
        });
        lrz(this.files[0], {
            width: 640
        }).then(function(rst) {
            var htmlLI=`
                <li class="one">
                    <img src="`+rst.base64+`">
                    <i class="icon iconfont icon-shijian J_deleteimg"></i>
                    <input value="`+rst.base64+`" type="hidden" name="[imgs]" class=""/>
                </li>`;
            $('#J_imgbox').append(htmlLI);
            layer.closeAll();
        });
    });
    // 删除图片
    $(document).on('click', '.J_deleteimg', function(event) {
        var $this=$(this);
        layer.open({
            title:[
                '确定删除此图片吗？',
                'background:rgba(0,0,0,0.99);color:rgba(255,255,255,0.9)'
            ],
            content: '<div><img style="max-height:6rem;max-width:100%;" src="'+$this.prev('img').attr('src')+'"/></div>',
            btn:['取消','确认'],
            anim:'scale',
            shade:true,
            yes:function(data){
                layer.closeAll();
            },
            no:function(data){
                $this.closest('li').remove();
            }
        });
    });
};






