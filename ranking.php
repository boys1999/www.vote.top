<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>投票</title>
    <meta name="format-detection" content="telephone=no">
    <meta content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" name="viewport">
    <meta name="apple-mobile-web-app-status-bar-style" content="black"/>
    <meta name="apple-mobile-web-app-title" content="">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link rel="stylesheet" href="public/qdd/mb/qdd.css">
    <link rel="stylesheet" href="vote/css/common.css">
    <link rel="stylesheet" href="vote/css/ranking.css">
    <link rel="stylesheet" href="vote/iconfont/iconfont.css" type="text/css" />
</head>

<body>

    <div id="vote">
        <!-- 公告 -->
        <div class="notice">
            <div class="row">
                <div class="left">公告：</div>
                <div class="right">
                    <i class="icon iconfont icon-shijian"></i>
                </div>
                <div class="center">
                    <marquee scrollAmount=5 width='100%'>酒吧横向公告酒吧横向吧横向公告1</marquee>
                </div>
            </div>
        </div>
        <!-- /公告 -->
        <!-- 图片轮播 -->
        <div class="y-addWrap">
            <div class="swipe" id="mySwipe">
                <div class="swipe-wrap">
                    <div v-for="(item,index) in swipeItem">
                        <a v-bind:href="item.href">
                            <img class="img-responsive" v-bind:src="item.img"/>
                        </a>
                    </div>
                </div>
            </div>
            <ul id="y-position">
                <li v-bind:class="[index == 0 ? 'cur' : '']" v-for="(item,index) in swipeItem"></li>
            </ul>
        </div>
        <!-- /图片轮播 -->
        <!-- 广告区1 -->
        <div class="adv1"></div>
        <!-- /广告区1 -->
        <!-- 搜索 -->
        <div class="search">
            <div class="row">
                <div class="right" @click="voteTips()">
                    <button>活动说明</button>
                </div>
                <div class="left">
                    <div class="input">
                        <input value="" placeholder="搜选手ID或名字" type="text" class=""/>
                        <button class="icon iconfont icon-shijian"></button>
                    </div>
                    <svg xmlns="http://www.w3.org/2000/svg" version="1.1" style=" width: 100%;height: 100%;top:0;left:0;z-index:0;position: absolute;">
                        <linearGradient id="grad1" x1="0%" y1="0%" x2="75%" y2="75%">
                            <stop offset="0%" style="stop-color:#0090ff;stop-opacity:1" />
                            <stop offset="50%" style="stop-color:#ef12de;stop-opacity:1" />
                            <stop offset="100%" style="stop-color:#ffd200;stop-opacity:1" />
                        </linearGradient>
                      <rect style="x:3px;y:1px;ry:1.25rem;rx:1.25rem; height:2.4rem;width:98%;fill:none;stroke-width:2px;stroke:url(#grad1)"/>
                    </svg>
                </div>
            </div>
        </div>
        <!-- /搜索 -->
        <!-- 赛况信息 -->
        <div class="votemsg shadow">
            <div class="row">
                <div class="group">
                    <div class="label">参赛人数</div>
                    <div class="value">5565</div>
                </div>
                <div class="group">
                    <div class="label">累计票数</div>
                    <div class="value">555511</div>
                </div>
                <div class="group">
                    <div class="label">综合人气指数</div>
                    <div class="value">555665</div>
                </div>
            </div>
            <div class="endtime">
                <span>距离投票结束:</span>
                <span>0</span><span>天</span>
                <span>0</span><span>时</span>
                <span>00</span><span>分</span>
                <span>0</span><span>秒</span>
            </div>
        </div>
        <!-- /赛况信息 -->

        <div class="ranking">
            <div class="hd">
                <div class="row">
                    <div class="left">
                        <span>排行榜</span>
                    </div>
                    <div class="right">
                        <div class="btns">
                            <a href="javascript:void(0)" class="left">·分组排行</a>
                            <a href="javascript:void(0)" class="left">·参赛排行</a>
                            <header style="clear: both;"></header>
                        </div>
                    </div>
                </div>
            </div>
            <div class="line gradientbg"></div>
            <ul>
                <li class="header">
                    <div class="index">名次</div>
                    <div class="logo">选手</div>
                    <div class="name">&nbsp;</div>
                    <div class="votes">票数</div>
                    <div class="viesw">人气</div>
                </li>
                <li>
                    <div class="index">
                        <span class="span">1</span>
                    </div>
                    <div class="logo">
                        <img src="https://img.alicdn.com/imgextra/i4/1945505012366163577/TB2NvyUeYFlpuFjy0FgXXbRBVXa_!!0-saturn_solar.jpg_640x640.jpg_.webp">
                    </div>
                    <div class="name">
                        <span class="span">马云马云马云马云</span>
                    </div>
                    <div class="votes">
                        <span class="span">1152</span>
                    </div>
                    <div class="viesw">
                        <span class="span">1152</span>
                    </div>
                </li>
                <li>
                    <div class="index">
                        <span class="span">1</span>
                    </div>
                    <div class="logo">
                        <img src="https://img.alicdn.com/imgextra/i4/1945505012366163577/TB2NvyUeYFlpuFjy0FgXXbRBVXa_!!0-saturn_solar.jpg_640x640.jpg_.webp">
                    </div>
                    <div class="name">
                        <span class="span">马云</span>
                    </div>
                    <div class="votes">
                        <span class="span">1152</span>
                    </div>
                    <div class="viesw">
                        <span class="span">1152</span>
                    </div>
                </li>
                <li>
                    <div class="index">
                        <span class="span">1222</span>
                    </div>
                    <div class="logo">
                        <img src="https://img.alicdn.com/imgextra/i4/1945505012366163577/TB2NvyUeYFlpuFjy0FgXXbRBVXa_!!0-saturn_solar.jpg_640x640.jpg_.webp">
                    </div>
                    <div class="name">
                        <span class="span">马云</span>
                    </div>
                    <div class="votes">
                        <span class="span">1152</span>
                    </div>
                    <div class="viesw">
                        <span class="span">1152</span>
                    </div>
                </li>
                <li>
                    <div class="index">
                        <span class="span">1</span>
                    </div>
                    <div class="logo">
                        <img src="https://img.alicdn.com/imgextra/i4/1945505012366163577/TB2NvyUeYFlpuFjy0FgXXbRBVXa_!!0-saturn_solar.jpg_640x640.jpg_.webp">
                    </div>
                    <div class="name">
                        <span class="span">马云</span>
                    </div>
                    <div class="votes">
                        <span class="span">1152</span>
                    </div>
                    <div class="viesw">
                        <span class="span">1152</span>
                    </div>
                </li>
            </ul>
        </div>


        <!-- 下拉更多 -->
        <div class="slidedown">
            <div class="label"><img src="public/img/01.gif">{{loadTips}}</div>
        </div>
        <!-- /下拉更多 -->

        <!-- 活动名称 -->
        <div class="activity">
            <div class="line gradientbg1"></div>
            <div class="label">活动名称</div>
        </div>
        <!-- /活动名称 -->

        <!-- 返回顶部 -->
        <div class="backtop" v-on:click="backtop()">
            <div class="wrap">
                <div class="label">返回顶部</div>
            </div>
        </div>
        <!-- /返回顶部 -->

        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>

        <!-- 底部导航 -->
        <div class="menu">
            <ul>
                <li v-for="(item,index) in menu">
                    <a v-bind:href="item.href">
                        <i class="icon iconfont" v-bind:class="item.icon"></i><br/>
                        <label>{{item.label}}</label>
                    </a>
                </li>
            </ul>
        </div>

        <!-- /底部导航 -->
    <script src="public/vue/vue.min.js" type="text/javascript"></script>
    <script src="public/axios/axios.min.js"></script>
    <script src="public/swipe/swipe.min.js"></script>
    <script src="public/layer_mobile/layer.js"></script>
    <script src="public/qdd/mb/qdd.js"></script>
    <!-- temp mock data -->
    <script src="public/mockjs/dist/mock-min.js" type="text/javascript"></script>
    <script src="public/mockjs/dist/mock-extend.js" type="text/javascript"></script>
    <script src="vote/js/mock.js" type="text/javascript"></script>
    <!-- /temp mock data -->
    <script src="vote/js/common.js" type="text/javascript"></script>
    <script src="vote/js/ranking.js" type="text/javascript"></script>
</body>

</html>
