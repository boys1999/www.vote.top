var QDD=(function(){
    // 前端公共资源路径
    var PUBLIC='public';//前带/，后不带/
    // 滚动条在Y轴上的滚动距离
    function get_scroll_top() {　　
        var scrollTop = 0,
            bodyScrollTop = 0,
            documentScrollTop = 0;　　
        if (document.body) {　　　　
            bodyScrollTop = document.body.scrollTop;　　
        }　　
        if (document.documentElement) {　　　　
            documentScrollTop = document.documentElement.scrollTop;　　
        }　　
        scrollTop = (bodyScrollTop - documentScrollTop > 0) ? bodyScrollTop : documentScrollTop;　　
        return scrollTop;
    }
    // 文档的总高度
    function get_scroll_height() {　　
        var scrollHeight = 0,
            bodyScrollHeight = 0,
            documentScrollHeight = 0;　　
        if (document.body) {　　　　
            bodyScrollHeight = document.body.scrollHeight;　　
        }　　
        if (document.documentElement) {　　　　
            documentScrollHeight = document.documentElement.scrollHeight;　　
        }　　
        scrollHeight = (bodyScrollHeight - documentScrollHeight > 0) ? bodyScrollHeight : documentScrollHeight;　　
        return scrollHeight;
    }
    // 浏览器视口的高度
    function get_window_height() {　　
        var windowHeight = 0;　　
        if (document.compatMode == "CSS1Compat") {　　　　
            windowHeight = document.documentElement.clientHeight;　　
        } else {　　　　
            windowHeight = document.body.clientHeight;　　
        }　　
        return windowHeight;
    }
    // 处理后端返回的json数据：json的排序、分页
    function json_sql(obj) {
        //初始化参数
        var options ={
            array:[],                       // 必需 目标数组
            sort:{                          // 非必(不填写不排序)
                field:'',                   //  排序字段
                order:'asc',                //  升降:asc|desc
            },
            page:{                          // 非必(不填写不分页)
                page:1,                     //  显示第n页
                perPage:0                  //  每页数量
            },
            where:{                         // 非必(不填写不查找)
                keyword:'',                //查找关键司,多个用逗号隔开，只有or条件
                field:''                    // 查找字段，多个用逗号隔开，只有or条件
            }
        };
        options = Object.assign(options,obj);
        var newArray=JSON.parse(JSON.stringify(options.array));
        // 查找关键词(模糊查找)
        function data_filter(){
            function is_exits(obj){
                var keyword=options.where.keyword.split(',');
                var field=options.where.field.split(',');
                var r=null;
                for (var i = 0; i < field.length; i++) {
                    for (var n = 0; n < keyword.length; n++) {
                        if (obj[field[i]].toString().indexOf(keyword[n])>-1) {
                            r=obj;
                            break;
                        };
                    };
                };
                return r;
            }
            var arrFilter=[];
            for (var i = 0; i < newArray.length; i++) {
                var arr=is_exits(newArray[i]);
                if (arr!=null) {
                    arrFilter.push(arr);
                };
            };
            newArray=arrFilter;
        };
        if (options.where.field.length>0) { data_filter(); };
        // 排序
        function data_sort(){
            newArray = newArray.sort(function(a, b) {
                var x = a[options.sort.field];
                var y = b[options.sort.field];
                if (options.sort.order=='desc') {
                    return ((y < x) ? -1 : ((y > x) ? 1 : 0));
                }else{
                    return ((x < y) ? -1 : ((x > y) ? 1 : 0));
                };
            });
        };
        if (options.sort.field.length>0) { data_sort(); }
        // 分页
        options.page.totalPage=1;// 总页数
        function data_page(){
            options.page.totalPage = Math.ceil(newArray.length/options.page.perPage);
            var start=options.page.perPage*(options.page.page-1);
            var end=options.page.perPage*(options.page.page);
            newArray=newArray.slice(start, end);
        };
        if (options.page.perPage>0) {   data_page(); };
        return {
            page:options.page.page,
            perPage:options.page.perPage,
            totalPage:options.page.totalPage,
            data:newArray
        };
    };
    // 提示页面
    function tips(obj){
        options={
            content:'',
            end:null,
            icon:'02_1.gif',
            style:'color:#fff;background:transparent;',
            shade:true,
            time:5,
            anim:'scale',// scale （默认）、up（从下往上弹出），如果不开启动画，设置false即可
            shadeClose: false
        };
        options = Object.assign(options,obj);
        layer.open({
            className:'J_qddtips',
            success:function(data){
                var $i=document.querySelectorAll('.J_qddtips i');
                for (var i = 0; i < $i.length; i++) {
                    $i[i].remove();
                };
            },
            content:'<div style="text-align:center;padding:1.2rem;border-radius:.5rem;'+options.style+'"><img src="'+PUBLIC+'/img/'+options.icon+'"><div>'+options.content+'</div>',
            shadeClose: options.shadeClose,
            shade:options.shade,
            type: 1,
            anim: options.anim,
            end:function(){
                if (typeof options.end == 'function') {
                    options.end();
                };
            },
            time:options.time
        });
    };
    // 首次加载，暂时放这
    tips({
        content:'小嗒奋力加载中…',
        shade:false,
        icon:'02_0.gif',
        style:'color:rgba(0,0,0,0.7);background:transparent;box-shadow:none;',
    });
    //数组交叉，解决css3瀑布流column布局  竖立布局不能横排问题
    function arr_crossover(arr){
        var a1=[];
        var a2=[];
        for (var i = 0; i < arr.length; i++) {
            if (i%2==0) {
                a1.push(arr[i]);
            }else{
                a2.push(arr[i]);
            };
        };
        return a1.concat(a2);
    }
    // 返回随机整数
    function random_num(Min,Max){
      var Range = Max - Min;
      var Rand = Math.random();
      return(Min + Math.round(Rand * Range));
    };
    return {
        jsonSql:json_sql,
        tips:tips,
        arrCrossover:arr_crossover,
        random:random_num,
        getScrollTop:get_scroll_top,
        getScrollHeight:get_scroll_height,
        getWindowHeight:get_window_height
    }
})();

// // 官网：http://layer.layui.com/mobile/
// layer.open({
//     title:[
//         '我是标题',
//         'background:rgba(0,0,0,0.3);'
//     ],
//     content: '<div style="background:#ff0000;">content</div>',
//     style:"background:#00ff00;",//整个背景颜色
//     btn:[
//         '取消',
//         '确认'
//     ],
//     anim:'scale',// scale （默认）、up（从下往上弹出），如果不开启动画，设置false即可
//     shade:true,// false 不显示遮罩,true显示
//     className:'myclassname',//自定义风格的class
//     shadeClose:true,//遮罩点击是否关闭
//     success:function(data){
//         // 层成功弹出层的回调
//         console.log(data);
//     },
//     yes:function(data){
//         // 第二个按钮事件
//         alert(    '第二个按钮事件'   );
//     },
//     no:function(data){
//         // 第二个按钮事件
//         alert(    '第一个按钮事件'   );
//     },
//     end:function(data){
//         // 层彻底销毁后的回调函数
//         alert(    '层彻底销毁后的回调函数'   );
//         layer.closeAll();//关闭所有弹出层
//     },
//     // type:1, //0带白色外框， 1只显示自定义content内容 ，2表示加载层
//     // skin: 'msg',//不写此属性：以 type属性为准，msg：半透明提示弹窗， footer:底部提问弹框
//     // time: 2
// });